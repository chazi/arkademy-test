<?php

/**
 * Untuk menjalankan file, 
 * Silahkan import dahulu file sql yang ada di ./db/work.sql kedalam database
 * File ini dijalankan dengan PHP 7.1 menggunakan Ampps
 * Database yang digunakan MySQL MariaDB
 */

// silahkan ganti password sesuai konfigurasi database server
$koneksi = mysqli_connect('localhost', 'root', 'mysql', 'work');

$sql = "SELECT 
        u.name AS user_name, 
        w.name AS work_name, 
        s.salary AS work_salary 
        FROM nama u 
        JOIN work w ON u.id_work = w.id 
        JOIN kategori s ON w.id_salary = s.id";
$query = mysqli_query($koneksi, $sql);
?>

<table border="1" cellpadding="10" cellspacing="0">
    <thead>
        <th>Name</th>
        <th>Work</th>
        <th>salary</th>
    </thead>
    <tbody>
        <?php while ($row = mysqli_fetch_assoc($query)): ?>
            <tr>
                <td><?= $row['user_name'] ?></td>
                <td><?= $row['work_name'] ?></td>
                <td>Rp. <?= number_format($row['work_salary'], 0, ',', '.') ?></td>
            </tr>
        <?php endwhile; ?>
    </tbody>
</table>
