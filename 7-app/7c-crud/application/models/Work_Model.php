<?php
defined('BASEPATH') OR die('No direct script access allowed!');

class Work_Model extends CI_Model
{
    public function getAll()
    {
        $this->db->select('u.*, u.id AS id_user, u.name AS user_name, w.name AS work_name, s.salary AS work_salary ');
        $this->db->join('work w', 'u.id_work = w.id');
        $this->db->join('kategori s', 'w.id_salary = s.id');
        $data = $this->db->get('nama u');
        return $data->result();
    }

    public function getSpecific($id)
    {
        $this->db->select('u.*, u.id AS user_id, u.name AS user_name, w.name AS work_name, s.salary AS work_salary ');
        $this->db->join('work w', 'u.id_work = w.id');
        $this->db->join('kategori s', 'w.id_salary = s.id');
        $this->db->where('u.id', $id);
        $data = $this->db->get('nama u');
        return $data->row();
    }

    public function getWork($id_work = 0)
    {
        if ($id_work !== 0) {
            $this->db->where('id', $id_work);
        }
        $data = $this->db->get('work');
        return $data->result();
    }

    public function getSallary($id_work = 0)
    {
        if ($id_work !== 0) {
            $work = $this->getWork($id_work)[0];
            $this->db->where('id', $work->id_salary);
        }
        $data = $this->db->get('kategori');
        return $data->result();
    }

    public function insertData($data)
    {
        $this->db->insert('nama', $data);
        return $this->getSpecific($this->db->insert_id());
    }

    public function updateData($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('nama', $data);
        return $this->getSpecific($id);
    }

    public function deleteData($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('nama');
    }
}

