<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Arkademy Bootcamp</title>

    <link rel="stylesheet" href="<?= base_url('../assets/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('../assets/icons/font-awesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('../assets/js/sweetalert/sweetalert.css') ?>">
</head>
<style>
    .img-logo {
        max-height: 50px;
    }
</style>
<body>
    <header class="mb-5 pb-5">
        <nav class="navbar bg-white shadow-sm">
            <div class="container">
                <a href="index.html"><img src="<?= base_url('../assets/img/arkademy-logo.png') ?>" alt="Gambar Logo" class="img-logo"></a>
            </div>
        </nav>
    </header>

    <section class="mt-5">
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex">
                    <div class="ml-auto text-right">
                        <button class="add btn btn-warning text-uppercase text-white shadow-sm" data-toggle="modal" data-target="#modalAdd">Add</button>
                    </div>
                </div>
                <div class="col-12 mt-2">
                    <table class="table table-bordered text-center">
                        <thead class="bg-light">
                            <th>Name</th>
                            <th>Work</th>
                            <th>Salary</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            <?php foreach ($all as $i => $v): ?>
                                <tr id="<?= ($v->id_user) ?>">
                                    <td><?= $v->user_name ?></td>
                                    <td data-work="<?= $v->id_work ?>"><?= $v->work_name ?></td>
                                    <td data-salary="<?= $v->id_salary ?>">Rp. <?= number_format($v->work_salary, 0, ',', '.') ?></td>
                                    <td>
                                        <a href="#" class="text-danger delete" data-toggle="modal" data-target="#modalDelete"><i class="fa fa-trash fa-2x"></i></a> &nbsp;&nbsp;&nbsp;
                                        <a href="#" class="text-success edit" data-toggle="modal" data-target="#modalEdit"><i class="fa fa-edit fa-2x"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAddLable" aria-hidden="true">
        <div class="modal-dialog" role="dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-uppercase" id="modalAddTitle">Add Data</h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span class="text-danger">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="post" id="formAdd" name="formAdd">
                        <div class="form-group">
                            <label for="addName">Name</label>
                            <input type="text" name="name" id="addName" class="form-control" placeholder="Name ...">
                        </div>
                        <div class="form-group">
                            <label for="addWork">Work</label>
                            <select name="work" id="addWork" class="form-control sel-work" placeholder="Work ...">
                                <option value="">Work ...</option>
                                <?php foreach ($works as $w): ?>
                                    <option value="<?= $w->id ?>"><?= $w->name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="addSalary">Salary</label>
                            <select name="salary" id="addSalary" class="form-control sel-sal" placeholder="Salary ...">
                                <option value="">Salary ...</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-warning text-uppercase text-white shadow-sm" id="insert" data-dismiss="modal">Add</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEditLable" aria-hidden="true">
            <div class="modal-dialog" role="dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title text-uppercase" id="modalEditTitle">Edit Data</h5>
                        <button class="close" data-dismiss="modal" aria-label="Close">
                            <span class="text-danger">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" id="formEdit" name="formEdit">
                            <div class="form-group">
                                <label for="editName">Name</label>
                                <input type="hidden" name="id" id="editId">
                                <input type="text" name="name" id="editName" class="form-control" placeholder="Name ...">
                            </div>
                            <div class="form-group">
                                <label for="editWork">Work</label>
                                <select name="work" id="editWork" class="form-control sel-work" placeholder="Work ...">
                                    <option value="">Work ...</option>
                                    <?php foreach ($works as $h): ?>
                                        <option value="<?= $h->id ?>"><?= $h->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="editSalary">Salary</label>
                                <select name="salary" id="editSalary" class="form-control sel-sal" placeholder="Salary ...">
                                    <option value="">Salary ...</option>
                                    <?php foreach ($salary as $s): ?>
                                        <option value="<?= $s->id ?>"><?= $s->salary ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-warning text-uppercase text-white shadow-sm" id="update" data-dismiss="modal">Update</button>
                    </div>
                </div>
            </div>
        </div>

    <script src="<?= base_url('../assets/js/jquery.js') ?>"></script>
    <script src="<?= base_url('../assets/js/bootstrap.bundle.min.js') ?>"></script>
    <script src="<?= base_url('../assets/js/sweetalert/sweetalert.min.js') ?>"></script>

    <script>
        $(document).ready(function() {
            $('body').on("click", '.edit', function() {
                let parentId = $(this).closest('tr').attr('id');
                let tdEl = $(this).closest('tr').children();
                let name = tdEl[0].innerText;
                let work = tdEl[1].getAttribute('data-work');
                let salary = tdEl[2].getAttribute('data-salary');
                
                $('#editName').val(name);
                $('#editId').val(parentId);
                $('#editWork').val(work);
                $('#editSalary').val(salary);
            });

            $('body').on('change', '.sel-work', function() {
                let idWork = $(this).val();
                $.ajax({
                    url: '<?= base_url('work/get_salary/') ?>' + idWork,
                    type: 'GET',
                    error: function(err) {
                        console.log(err);
                    },
                    success: function(res) {
                        $('.sel-sal').html(res);
                    }
                });
            });
            
            $('#insert').click(function() {
                let form = document.getElementById('formAdd');
                let formData = new FormData(form);
                // let id = Number($('tbody tr').last().attr('id')) + 1;
                
                $.ajax({
                    url: '<?= base_url('work/store') ?>',
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    error: function(err) {
                        console.log(err);
                    },
                    success: function(res) {
                        id = res.data.user_id;
                        let template = `<tr id=${id}>`;
                        $.each(res.data, function(k, v) {
                            if (v !== id) {
                                if (k == 'work_salary') {
                                    template += `<td data-${k.replace('_name', '')}="${res.data.id_salary}">Rp. ${v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</td>`;
                                } else if (k == 'work_name') {
                                    template += `<td data-${k.replace('_name', '')}="${res.data.id_work}">${v}</td>`;
                                } else if (k == 'user_name') {
                                    template += `<td>${v}</td>`;
                                }
                            }
                        });

                        template += `
                                <td>
                                    <a href="#" class="text-danger delete" data-toggle="modal" data-target="#modalDelete"><i class="fa fa-trash fa-2x"></i></a> &nbsp;&nbsp;&nbsp;
                                    <a href="#" class="text-success edit" data-toggle="modal" data-target="#modalEdit"><i class="fa fa-edit fa-2x"></i></a>
                                </td>
                            </tr>
                        `;

                        $('tbody').append(template);
                        swal(res.title, res.msg, res.title);
                    }
                });
            });

            $('#update').click(function() {
                let form = document.getElementById('formEdit');
                let formData = new FormData(form);
                let rowId = formData.get('id');

                $.ajax({
                    url: '<?= base_url('work/update') ?>',
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    error: function(err) {
                        console.log(err);
                    },
                    success: function(res) {
                        let template = ``;
                        $.each(res.data, function(k, v) {
                            if (v !== rowId) {
                                if (k == 'work_salary') {
                                    template += `<td data-${k.replace('_name', '')}="${res.data.id_salary}">Rp. ${v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</td>`;
                                } else if (k == 'work_name') {
                                    template += `<td data-${k.replace('_name', '')}="${res.data.id_work}">${v}</td>`;
                                } else if (k == 'user_name') {
                                    template += `<td>${v}</td>`;
                                }
                            }
                        });

                        template += `
                            <td>
                                <a href="#" class="text-danger delete" data-toggle="modal" data-target="#modalDelete"><i class="fa fa-trash fa-2x"></i></a> &nbsp;&nbsp;&nbsp;
                                <a href="#" class="text-success edit" data-toggle="modal" data-target="#modalEdit"><i class="fa fa-edit fa-2x"></i></a>
                            </td>
                        `;

                        $('#'+rowId).html(template);
                        swal(res.title, res.msg, res.title);
                    }
                });
            });

            $('body').on('click', '.delete', function() {
                let parent = $(this).closest('tr');
                let name = parent.children()[0].innerHTML;
                console.log(parent.attr('id'));

                $.ajax({
                    url: '<?= base_url('work/destroy/') ?>' + parent.attr('id'),
                    error: function(err) {
                        console.log(err);
                    },
                    success: function(res) {
                        parent.remove();
                        console.log(res.title);
                        swal(res.title, res.msg, res.title);
                    }
                })
            })
        })
    </script>
</body>
</html>