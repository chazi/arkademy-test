<?php
defined('BASEPATH') OR die('No direct script access allowed');

class Work extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Work_Model', 'work');
    }

    public function index()
    {
        $data['all'] = $this->work->getAll();
        $data['salary'] = $this->work->getSallary();
        $data['works'] = $this->work->getWork();
        $this->load->view('data', $data);
    }

    public function get_salary()
    {
        $idWork = $this->uri->segment(3);
        $data = $this->work->getSallary($idWork);
        $options = '';
        foreach ($data as $v) {
            $options .= "<option value=\"$v->id\">$v->salary</option>";
        }

        echo $options;
    }

    public function store()
    {
        header('Content-Type: application/json');
        $post = $this->input->post();
        $data = [
            'name' => $post['name'],
            'id_work' => $post['work'],
            'id_salary' => $post['salary']
        ];
        $result = (array) $this->work->insertData($data);
        if ($result) {
            $return = [
                'title' => 'success',
                'data' => $result,
                'msg' => 'Data berhasil ditambahkan!'
            ];
        } else {
            $return = [
                'title' => 'error',
                'msg' => 'Something wrong!'
            ];
        }

        echo json_encode($return);
    }

    public function update()
    {
        header('Content-Type: application/json');
        $post = $this->input->post();
        $id = $post['id'];
        $data = [
            'name' => $post['name'],
            'id_work' => $post['work'],
            'id_salary' => $post['salary']
        ];
        $result = (array) $this->work->updateData($data, $id);
        if ($result) {
            $return = [
                'title' => 'success',
                'data' => $result,
                'msg' => 'Data ' . $post['name'] . ' berhasil diubah!'
            ];
        } else {
            $return = [
                'title' => 'error',
                'msg' => 'Something wrong!'
            ];
        }

        echo json_encode($return);
    }

    public function destroy()
    {
        header('Content-Type: application/json');
        $id = $this->uri->segment(3);
        $data = $this->work->getSpecific($id);
        $result = $this->work->deleteData($id);
        if ($result) {
            $return = [
                'title' => 'success',
                'msg' => 'Data ' . $data->user_name . ' berhasil dihapus!'
            ];
        } else {
            $return = [
                'title' => 'error',
                'msg' => 'Something wrong!'
            ];
        }

        echo json_encode($return);
    }
}

