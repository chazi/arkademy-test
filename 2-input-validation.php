<?php

function is_username_valid($username)
{
    if (preg_match("/^(?=.*[a-zA-Z])[A-Za-z0-9](?=.*[0-9]).{5,9}/", $username)) {
        return true;
    }

    return false;
}

function is_password_valid($password)
{
    if (preg_match("/(?=.*?[A-Za-z])(?=.*?[0-9])(?=.*?[\=]).{8,}/", $password)) {
        return true;
    }

    return false;
}

var_dump(is_username_valid('1Diana'));
// var_dump(is_password_valid('passW0r$d='));
