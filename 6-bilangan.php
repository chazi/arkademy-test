<?php

function mrBrulee($opt, $n, $list)
{
    $ret = 0;
    $lists = '';
    for ($i=1; $i <= $n; $i++) { 
        $lists .= $i;
    }
    $arrList = str_split($lists);

    for ($k=0; $k < count($list); $k++) { 
        if ($k == 0) {
            $ret = $arrList[$list[$k] - 1];
        } else {
            if ($opt == 'SUM') {
                $ret = $ret + $arrList[$list[$k] - 1];
            } elseif ($opt == 'MUL') {
                $ret = $ret * $arrList[$list[$k] - 1];
            } elseif ($opt == 'SUB') {
                $ret = $ret - $arrList[$list[$k] - 1];
            } elseif ($opt == 'DIV') {
                $ret = $ret / $arrList[$list[$k] - 1];
            }
        }
    }

    echo $ret;
}

mrBrulee('MUL', 20, [13, 15, 17]);
