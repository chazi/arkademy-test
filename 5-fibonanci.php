<?php

function fibonanci($num, $cond)
{
    $sum = 0;
    $i = 1;
    $b = 1;
    while ($i <= $num) {
        if ($cond == 'even') {
            if ($i%2 == 0) {
                $sum += $i;
            }
        } elseif ($cond == 'odd') {
            if ($i%2 !== 0) {
                $sum += $i;
            }
        }
        $a = $i;
        $i += $b;
        $b = $a;
    }

    echo $sum;
}

function evenSum($num)
{
    return fibonanci($num, 'even');
}

function oddSum($num)
{
    return fibonanci($num, 'odd');
}

// evenSum(1000);
oddSum(1000);
