## Jawaban Test Arkademy Bootcamp Batch 11 - 4

### Syntax yang saya gunakan adalah :
- PHP
- MySQl
- Javascript
> Mysql dan javascript hanya digunakan pada jawaban soal no 7
  

### Aplikasi yang diperlukan untuk menjalankan program:

- Web server PHP dan MySQl, seperti AMPPS, XAMPP, WAMM
- CMD / Terminal
- Browser


### Aplikasi Crud
1. Aplikasi menggunakan framework Codeigniter 3.10 <br>
2. Folder aplikasi berada di `./7-app/7c-crud/` <br>
3. Untuk database bisa diimport dari file `./7-app/database/work.sql` <br>
4. Ubah konfigurasi database di `./7-app/7c-crud/application/config/database.php` <br>


### Kegunaan JSON untuk REST API
Penggunaan JSON dalam REST API adalah sebagai media pertukaran data antara server dan client.
JSON dipilih karena bisa digunakan di hampir semua bahasa pemrograman dan penulisan yang mudah


### ScreenShoot App Crud

- Index ![index](7-app/assets/img/screenshoot/index.png)

- Add ![add](7-app/assets/img/screenshoot/add.png)

- Edit ![edit](7-app/assets/img/screenshoot/edit.png)

- Delete ![delete](7-app/assets/img/screenshoot/delete.png)

  