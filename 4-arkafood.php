<?php

function arkaFood($nominal, $kode_vocher, $jarak, $pajak = false)
{
    $diskon = 0;
    if ($kode_vocher !== 'false') {
        if ($kode_vocher == 'ARKAFOOD5') {
            if ($nominal >= 50000) {
                $potongan = (50/100) * $nominal;
                if ($potongan <= 50000) {
                    $diskon = $potongan;
                }
            }
        } elseif ($kode_vocher == 'DITRAKTIRDEMY') {
            if ($nominal >= 25000) {
                $potongan = (60/100) * $nominal;
                if ($potongan <= 30000) {
                    $diskon = $potongan;
                }
            }
        }
    }

    $ongkir = 5000;
    $jarak = round(($jarak - 1.5));
    for ($i=0; $i < $jarak; $i++) { 
        $ongkir += 3000;
    }

    if ($pajak == true) {
        $pajak = (5/100) * $nominal;
    } else {
        $pajak = 0;
    }

    $total = ($nominal - $diskon) + $ongkir + $pajak;
    echo 'Total : Rp. ' . number_format($total, 0, ',', '.');
}

arkaFood(75000,'ARKAFOOD5',5,false);